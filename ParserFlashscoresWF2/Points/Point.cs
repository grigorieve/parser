﻿namespace ParserFlashscoresWF2
{
    internal class Point
    {
        internal int WhoWin;
        internal int WhoServe;

        public Point(int whoWin, int whoServe)
        {
            WhoWin = whoWin;
            WhoServe = whoServe;
        }

        public Point()
        {
        }
    }
}