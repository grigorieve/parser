﻿using System.Collections.Generic;

namespace ParserFlashscoresWF2
{
    internal class Set
    {
        public int G1;
        public int G2;
        public List<Game> Games = new List<Game>();
        public int WhoWin;
    }
}