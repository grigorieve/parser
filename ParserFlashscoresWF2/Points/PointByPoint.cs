﻿using System.Collections.Generic;

namespace ParserFlashscoresWF2
{
    internal class PointByPoint
    {
        public int S1;
        public int S2;

        public List<Set> Sets = new List<Set>();

        //public PointByPoint(int s1,  int s2)
        //{
        //    S1 = s1;
        //    S2 = s2;
        //    sets = new Set[S1 + S2];
        //}

        public int Retired = 0; //отказ одного из игроков продолжить матч

        public int WhoWin;
        //public int WhoServe;
    }
}