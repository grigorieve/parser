﻿using System.Collections.Generic;

namespace ParserFlashscoresWF2
{
    internal class Game
    {
        public int P1;
        public int P2;
        public List<int> PointsInt = new List<int>();
        public List<Point> Points = new List<Point>();
        public string PointsString;
        public int WhoWin;
        public int WhoServe;

        public Game(Game g)
        {
            P1 = g.P1;
            P2 = g.P2;
            PointsInt = g.PointsInt;
            Points = g.Points;
            PointsString = g.PointsString;
            WhoWin = g.WhoWin;
            WhoServe = g.WhoServe;
        }

        public Game()
        {
        }
    }
}