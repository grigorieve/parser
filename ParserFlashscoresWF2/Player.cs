﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserFlashscoresWF2
{
    class Player
    {
        public string Name { get; set; }
        public string url { get; set; }

        public List<Statistics> Statistics = new List<Statistics>();

        //public Player(string name)
        //{
        //    Name = name;
        //}
    }
}
