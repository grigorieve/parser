﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParserFlashscoresWF2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnMatchSummary_Click(object sender, EventArgs e)
        {
            tbResultPars.Text = ParserInMatch.MatchSummary(lblID.Text);
        }

        private void btnMatchStatistics_Click(object sender, EventArgs e)
        {
            tbResultPars.Text = ParserInMatch.MatchStatistics(lblID.Text);
        }

        private void btnPointByPoont_Click(object sender, EventArgs e)
        {
            tbResultPars.Text = ParserInMatch.PointByPoont(lblID.Text);
        }

        private void btnOdds_Click(object sender, EventArgs e)
        {
            tbResultPars.Text = ParserInMatch.Odds(lblID.Text);
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            // Надо закрыть форму, браузер...
        }

        private void btnCategories_Click(object sender, EventArgs e)
        {
            tbResultPars.Text = ParserInMatch.ParsCategories();
        }

        private void btnCity_Click(object sender, EventArgs e)
        {
            tbResultPars.Text = ParserInMatch.ParsCity();
        }

        private void btnMatches_Click(object sender, EventArgs e)
        {
            tbResultPars.Text = ParserInMatch.ParsMatches();
        }

        private void btnGetID_Click(object sender, EventArgs e)
        {
            if (tbEnterUrl.Text.Contains("/match/"))
            {
                string id;
                id = tbEnterUrl.Text.Substring(tbEnterUrl.Text.IndexOf("/match/") + 7);
                id = id.Substring(0, 8);

                lblID.Text = id;
            }
        }

        private void btnAllCity_Click(object sender, EventArgs e)
        {
            tbResultPars.Text = ParserInMatch.ParsAllCitys();
        }

        private void btnCreateClass_Click(object sender, EventArgs e)
        {
            ParsFromClass.CreateMatchClass(lblID.Text);
            tbResultPars.Text += "Готово!!!";
        }

        private void tbEnterUrl_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
