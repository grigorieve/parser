﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ParserFlashscoresWF2
{
    class Stage //стадия игры (матч, сет...)
    {
        string Name;
        public List<Market> markets = new List<Market>();

        static List<IWebElement> marketsElements;

        private Regex regex1 = new Regex(@"\('(.*?)'");
        private Regex regex2 = new Regex(@" '(.*?)'\)");

        public Stage(IWebDriver browser, IWebElement s)
        {
            s.Click();
            Name = s.Text;
            //Thread.Sleep(1000);

            WebDriverWait wait = new WebDriverWait(browser, TimeSpan.FromSeconds(10));
            wait.Until(driver => driver.FindElements(By.CssSelector("a")).Count > 0);
            string atrOnclick = s.FindElement(By.CssSelector("a")).GetAttribute("onclick");
            string market = regex1.Match(atrOnclick).Groups[1].Value;
            string stage = regex2.Match(atrOnclick).Groups[1].Value;
            string selector = "#block-" + market + "-" + stage + " .sortable";
            marketsElements = browser.FindElements(By.CssSelector(selector)).ToList();


            if (market == "moneyline" || market == "oddeven")
            {
                foreach (var m in marketsElements) //он здесь один
                {
                    markets.Add(new Market(market, m));
                }
            }
            else if (market == "under-over" || market == "asian-handicap" || market == "correct-score")
            {
                foreach (var m in marketsElements)
                {
                    markets.Add(new Market(m));
                }
            }
            else { } // такого быть не должно (в теннисе)!! надо добавить эксэпшн


        }

        //public Stage(string name)
        //{
        //    Name = name;
        //}

        //public void AddMarket(string name)
        //{
        //    markets.Add(new Market(name));
        //}
    }
}
