﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace ParserFlashscoresWF2
{
    class UnitOdds //базовая единица
    {
        string Bookmeker;
        static List<IWebElement> CoefsElement;
        List<string> CoefS = new List<string>();
        public List<double> CoefD = new List<double>();
        double c;

        public UnitOdds(IWebElement u)
        {
            Bookmeker = u.FindElement(By.CssSelector(".bookmaker a")).GetAttribute("title");
            CoefsElement = u.FindElements(By.CssSelector(".kx")).ToList();
            for (int i = 0; i < CoefsElement.Count; i++)
            {
                CoefS.Add(CoefsElement[i].Text);

                if (double.TryParse(CoefsElement[i].Text.Replace(".", ","), out c)) CoefD.Add(c);
                else CoefD.Add(0);
            }
        }

        //public UnitOdds(string bookmeker, double coef1, double coef2)
        //{
        //    Bookmeker = bookmeker;
        //    Coef1 = coef1;
        //    Coef2 = coef2;
        //}
    }
}
