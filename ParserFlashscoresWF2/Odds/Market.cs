﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserFlashscoresWF2
{
    class Market //маркет
    {
        string Name;
        static List<IWebElement> unitsElements;

        public List<UnitOdds> units = new List<UnitOdds>();

        public Market(string name, IWebElement s)
        {
            Name = name;
            unitsElements = s.FindElements(By.CssSelector("tbody tr")).ToList();
            foreach (var u in unitsElements)
            {
                units.Add(new UnitOdds(u));
            }
        }

        public Market(IWebElement m)
        {
            string sg = m.FindElement(By.CssSelector(".unsortable")).Text;
            List<IWebElement> el3 = m.FindElements(By.CssSelector("td")).ToList();
            string kol = el3[1].Text;
            //Как заменить предыдущие 2 строчки, чтобы сразу вытаскивать знчение Text из элементов
            //string[] kolm = m.FindElements(By.CssSelector("td")).ToList();
            Name = sg + " " + kol;

            unitsElements = m.FindElements(By.CssSelector("tbody tr")).ToList();
            foreach (var u in unitsElements)
            {
                units.Add(new UnitOdds(u));
            }

        }
    }
}
