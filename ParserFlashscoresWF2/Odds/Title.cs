﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ParserFlashscoresWF2
{
    internal class OddsTitle // рынок (12, тотал, фора, гандикап...)
    {
        static List<IWebElement> stagesElements;

        internal string Name;
        internal List<Stage> stages = new List<Stage>();

        public OddsTitle(IWebDriver browser, IWebElement t)
        {
            t.Click();
            Name = t.Text;
            //Thread.Sleep(1000);

            string selector = ".odds-scope-bookmark [id^=" + t.GetAttribute("id") + "]";
            WebDriverWait wait = new WebDriverWait(browser, TimeSpan.FromSeconds(10));
            wait.Until(driver => driver.FindElements(By.CssSelector(selector)).Count > 0);
            stagesElements = browser.FindElements(By.CssSelector(selector)).ToList();

            foreach (var s in stagesElements)
            {
                stages.Add(new Stage(browser, s));
            }
        }

        //public OddsTitle(string name)
        //{
        //    Name = name;
        //}

        //public void AddStage(string name)
        //{
        //    stages.Add(new Stage(name));
        //}
    }
}
