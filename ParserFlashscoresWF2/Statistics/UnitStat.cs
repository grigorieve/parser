﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ParserFlashscoresWF2
{
    public class UnitStat
    {
        public int Total;
        public int Part;
        public int Percentage;
        //{
        //    get
        //    {
        //        if (Total == 0) return 0;
        //        else return Part * 100 / Total;
        //    }
        //    set { Percentage = value; }
        //}

        Regex regex;

        //public UnitStat(int part, int total)
        //{
        //    Total = total;
        //    Part = part;
        //}

        public UnitStat(int percenage, int part, int total)
        {
            Total = total;
            Part = part;
            Percentage = percenage;
        }

        public UnitStat(string stat)
        {
            regex = new Regex(@"(\d*)%");
            Percentage = Convert.ToInt32(regex.Match(stat).Groups[1].Value);
            regex = new Regex(@"\((\d*)/");
            Part = Convert.ToInt32(regex.Match(stat).Groups[1].Value);
            regex = new Regex(@"/(\d*)");
            Total = Convert.ToInt32(regex.Match(stat).Groups[1].Value);
        }
    }
}
