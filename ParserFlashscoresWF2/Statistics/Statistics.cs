﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserFlashscoresWF2
{
    public class Statistics
    {
        //Service
        public UnitStat Aces;
        public UnitStat DoubleFaults;
        public UnitStat FirstServePercentage; //либо int в целых процентах
        public UnitStat FirstServePointsWon;
        public UnitStat SecondServePointsWon;
        public UnitStat BreakPointsSaved;

        //Return
        public UnitStat FirstReturnPointsWon;
        public UnitStat SecondReturnPointsWon;
        public UnitStat BreakPointsConverted;

        //Points
        public UnitStat Winners;
        public UnitStat UnforcedErrors;
        public int MaxPointsInRow;
        public UnitStat ServicePointsWon;
        public UnitStat ReturnPointsWon;
        public UnitStat TotalPointsWon;

        //Games
        public int MaxGamesInRow;
        public UnitStat ServiceGamesWon;
        public UnitStat ReturnGamesWon;
        public UnitStat TotalGamesWon;

        //Time Из вкладки Summary //Может пусть там и остается
        //public TimeSpan Time;

        public Statistics() {}

        public Statistics (Statistics s)
        {
            Aces = s.Aces;
            DoubleFaults = s.DoubleFaults;
            FirstServePercentage = s.FirstServePercentage; 
            FirstServePointsWon = s.FirstServePointsWon;
            SecondServePointsWon = s.SecondServePointsWon;
            BreakPointsSaved = s.BreakPointsSaved;

            FirstReturnPointsWon = s.FirstReturnPointsWon;
            SecondReturnPointsWon = s.SecondReturnPointsWon;
            BreakPointsConverted = s.BreakPointsConverted;

            Winners = s.Winners;
            UnforcedErrors = s.UnforcedErrors;
            MaxPointsInRow = s.MaxPointsInRow;
            ServicePointsWon = s.ServicePointsWon;
            ReturnPointsWon = s.ReturnPointsWon;
            TotalPointsWon = s.TotalPointsWon;

            MaxGamesInRow = s.MaxGamesInRow;
            ServiceGamesWon = s.ServiceGamesWon;
            ReturnGamesWon = s.ReturnGamesWon;
            TotalGamesWon = s.TotalGamesWon;
        }
    }
}
