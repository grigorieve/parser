﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ParserFlashscoresWF2
{
    class ParsPointByPoint
    {
        static List<IWebElement> titelGames;
        static List<IWebElement> gamePoints;
        static List<IWebElement> serve;
        static string text = "";

        public static PointByPoint PBP(IWebDriver browser, int num, int[,] scoreOnGame, string name1, string name2)
        {
            PointByPoint P = new PointByPoint();

            // Установка счета по сетам.
            titelGames = browser.FindElements(By.CssSelector(".scoreboard")).ToList();
            P.S1 = int.Parse(titelGames[0].Text);
            P.S2 = int.Parse(titelGames[1].Text);

            // Проверка, закончен ли матч досрочно.
            string text = browser.FindElement(By.ClassName("match-info")).Text;
            if (text.Contains(name1)) P.Retired = 1;
            else if (text.Contains(name2)) P.Retired = 2;

            // Определение победителя, с учетом было ли досрочное завершение.
            P.WhoWin = 1;
            if (P.Retired == 1 || P.S2 > P.S1) P.WhoWin = 2;

            // Поочередный парс сетов.
            for (int i = 1; i <= num; i++)
            {
                browser.FindElement(By.Id("mhistory-" + i + "-history")).Click();
                //Thread.Sleep(1000);

                P.Sets.Add(ParsSet(browser, i, scoreOnGame));
            }

            return P;
        }

        static Set ParsSet(IWebDriver browser, int num, int[,] sg)
        {
            Set set = new Set();

            set.G1 = sg[0, num - 1];
            set.G2 = sg[1, num - 1];

            set.WhoWin = 1;
            if (sg[0, num - 1] < sg[1, num - 1]) set.WhoWin = 2;

            WebDriverWait wait = new WebDriverWait(browser, TimeSpan.FromSeconds(10));
            wait.Until(driver => driver.FindElements(By.CssSelector("#tab-mhistory-" + num + "-history .fifteens_available")).Count > 0);

            titelGames = browser.FindElements(By.CssSelector("#tab-mhistory-" + num + "-history .fifteens_available")).ToList();
            gamePoints = browser.FindElements(By.CssSelector("#tab-mhistory-" + num + "-history .fifteen")).ToList();

            for (int i = 0; i < titelGames.Count; i++)
            {
                Game g = new Game();

                // Кто подавал в гейме
                serve = titelGames[i].FindElements(By.CssSelector(".server")).ToList();
                g.WhoServe = 1;
                if (serve[0].FindElements(By.ClassName("icon-box")).Count == 0) g.WhoServe = 2;

                // Кто выиграл гейм
                if (!titelGames[i].Text.Contains("LOST SERVE")) g.WhoWin = g.WhoServe;
                else if (g.WhoServe == 1) g.WhoWin = 2;
                else g.WhoWin = 1;

                g.PointsString = gamePoints[i].Text;

                // Заполнение очередности выигрывания очков и массива Point'ов
                text = String.Join("", g.PointsString.Split(' ', 'M', 'S', 'B', 'P'));
                char[] r = new char[] { ':', ',' };
                string[] p = text.Split(r, StringSplitOptions.RemoveEmptyEntries);

                if (p[0] == "0")
                {
                    g.PointsInt.Add(2);
                    g.Points.Add(new Point(2, g.WhoServe));
                }
                else
                {
                    g.PointsInt.Add(1);
                    g.Points.Add(new Point(1, g.WhoServe));
                }

                int j = 2;
                for (; j < p.Length && j < 14; j += 2) //до А +1
                {
                    if (p[j] == p[j - 2])
                    {
                        g.PointsInt.Add(2);
                        g.Points.Add(new Point(2, g.WhoServe));
                    }
                    else
                    {
                        g.PointsInt.Add(1);
                        g.Points.Add(new Point(1, g.WhoServe));
                    }
                }

                for (; j < p.Length; j += 2) //для А
                {
                    if (p[j] == "A" || p[j - 1] == "A")
                    {
                        g.PointsInt.Add(1);
                        g.Points.Add(new Point(1, g.WhoServe));
                    }
                    else
                    {
                        g.PointsInt.Add(2);
                        g.Points.Add(new Point(2, g.WhoServe));
                    }
                }

                if (g.WhoWin == 1)
                {
                    g.PointsInt.Add(1);
                    g.Points.Add(new Point(1, g.WhoServe));
                }
                else
                {
                    g.PointsInt.Add(2);
                    g.Points.Add(new Point(2, g.WhoServe));
                }

                // Кто сколько очков выиграл в гейме.
                g.P1 = 2 * g.PointsInt.Count - g.PointsInt.Sum();
                g.P2 = g.PointsInt.Sum() - g.PointsInt.Count;

                set.Games.Add(new Game(g));
            }

            // Заполнение ТБ, если он есть.
            if (sg[0, num - 1] + sg[1, num - 1] == 13)
            {
                string selector = "#tab-mhistory-" + num + "-history td";
                titelGames = browser.FindElements(By.CssSelector(selector)).ToList();

                // Ищем начало ТБ
                // Это плохо, очень много времени уходит на проверку списка. Как оптимизировать? пока не знаю...
                int i = 0;
                for (; i < titelGames.Count; i++)
                {
                    if (titelGames[i].Text.Contains("Tiebreak")) break;
                }

                Game tb = new Game();
                tb.PointsString = "";

                i++;
                for (; i < titelGames.Count; i += 5)
                {
                    Point p = new Point();

                    p.WhoServe = 1;
                    if (titelGames[i + 1].FindElements(By.ClassName("icon-box")).Count == 0) p.WhoServe = 2;

                    if (!titelGames[i].Text.Contains("LOST") && !titelGames[i + 4].Text.Contains("LOST"))
                    {
                        p.WhoWin = p.WhoServe;
                        tb.PointsInt.Add(p.WhoServe);
                    }
                    else if (p.WhoServe == 1)
                    {
                        p.WhoWin = 2;
                        tb.PointsInt.Add(2);
                    }
                    else
                    {
                        p.WhoWin = 1;
                        tb.PointsInt.Add(1);
                    }

                    tb.PointsString += titelGames[i + 2].Text + ", ";

                    tb.Points.Add(p);
                }

                // Количество выигранных очков каждым игроком.
                tb.P1 = tb.P2 = 0;
                for (int j = 0; j < tb.Points.Count; j++)
                {
                    if (tb.Points[j].WhoWin == 1) tb.P1++;
                    else tb.P2++;
                }

                // Кто выиграл ТБ
                tb.WhoWin = tb.P1 > tb.P2 ? 1 : 2;

                set.Games.Add(new Game(tb));
            }

            return set;
        }
    }
}
