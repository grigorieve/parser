﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace ParserFlashscoresWF2
{
    class ParsStatistics
    {
        static List<IWebElement> elements;

        public static void Player(IWebDriver browser, int countOfSet, out Player Player1, out Player Player2)
        {
            Player1 = new Player();
            Player2 = new Player();

            elements = browser.FindElements(By.CssSelector(".tname__text a")).ToList();
            Player1.Name = elements[0].Text;
            Player2.Name = elements[1].Text;

            Regex regex = new Regex(@"'(.*)'");
            Player1.url = regex.Match(elements[0].GetAttribute("onclick")).Value;
            Player2.url = regex.Match(elements[1].GetAttribute("onclick")).Value;

            WebDriverWait wait = new WebDriverWait(browser, TimeSpan.FromSeconds(10));

            for (int i = 0; i <= countOfSet; i++)
            {
                if (i != 0) browser.FindElement(By.Id("statistics-" + i + "-statistic")).Click();

                string selector = "#tab-statistics-" + i + "-statistic .statRow";
                wait.Until(driver => driver.FindElements(By.CssSelector(selector)).Count > 0);
                elements = browser.FindElements(By.CssSelector(selector)).ToList();

                Part(elements, out Statistics s1, out Statistics s2);
                Player1.Statistics.Add(s1);
                Player2.Statistics.Add(s2);
            }
        }

        static void Part(List<IWebElement> elements, out Statistics s1, out Statistics s2)
        {
            s1 = new Statistics();
            s2 = new Statistics();
            string text;

            // Поля не требующие дополнительных вычислений.
            foreach (var item in elements)
            {
                text = item.Text;

                if (text.Contains("1st Serve Points Won"))
                {
                    s1.FirstServePointsWon = new UnitStat(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    s2.FirstServePointsWon = new UnitStat(item.FindElement(By.ClassName("statText--awayValue")).Text);
                }

                else if (text.Contains("2nd Serve Points Won"))
                {
                    s1.SecondServePointsWon = new UnitStat(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    s2.SecondServePointsWon = new UnitStat(item.FindElement(By.ClassName("statText--awayValue")).Text);
                }

                else if (text.Contains("Break Points Saved"))
                {
                    s1.BreakPointsSaved = new UnitStat(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    s2.BreakPointsSaved = new UnitStat(item.FindElement(By.ClassName("statText--awayValue")).Text);
                }

                else if (text.Contains("1st Return Points Won"))
                {
                    s1.FirstReturnPointsWon = new UnitStat(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    s2.FirstReturnPointsWon = new UnitStat(item.FindElement(By.ClassName("statText--awayValue")).Text);
                }

                else if (text.Contains("2nd Return Points Won"))
                {
                    s1.SecondReturnPointsWon = new UnitStat(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    s2.SecondReturnPointsWon = new UnitStat(item.FindElement(By.ClassName("statText--awayValue")).Text);
                }

                else if (text.Contains("Break Points Converted"))
                {
                    s1.BreakPointsConverted = new UnitStat(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    s2.BreakPointsConverted = new UnitStat(item.FindElement(By.ClassName("statText--awayValue")).Text);
                }

                else if (text.Contains("Service Points Won"))
                {
                    s1.ServicePointsWon = new UnitStat(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    s2.ServicePointsWon = new UnitStat(item.FindElement(By.ClassName("statText--awayValue")).Text);
                }

                else if (text.Contains("Return Points Won"))
                {
                    s1.ReturnPointsWon = new UnitStat(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    s2.ReturnPointsWon = new UnitStat(item.FindElement(By.ClassName("statText--awayValue")).Text);
                }

                else if (text.Contains("Total Points Won"))
                {
                    s1.TotalPointsWon = new UnitStat(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    s2.TotalPointsWon = new UnitStat(item.FindElement(By.ClassName("statText--awayValue")).Text);
                }

                else if (text.Contains("Service Games Won"))
                {
                    s1.ServiceGamesWon = new UnitStat(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    s2.ServiceGamesWon = new UnitStat(item.FindElement(By.ClassName("statText--awayValue")).Text);
                }

                else if (text.Contains("Return Games Won"))
                {
                    s1.ReturnGamesWon = new UnitStat(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    s2.ReturnGamesWon = new UnitStat(item.FindElement(By.ClassName("statText--awayValue")).Text);
                }

                else if (text.Contains("Total Games Won"))
                {
                    s1.TotalGamesWon = new UnitStat(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    s2.TotalGamesWon = new UnitStat(item.FindElement(By.ClassName("statText--awayValue")).Text);
                }
            }

            // Оставшиеся поля, все поля статистики переделываем под UnitStat.
            int Percent, Part, Total;

            foreach (var item in elements)
            {
                text = item.Text;

                if (text.Contains("Aces"))
                {
                    Part = Convert.ToInt32(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    Total = s1.ServicePointsWon.Total;
                    Percent = Part * 100 / Total;
                    s1.Aces = new UnitStat(Percent, Part, Total);

                    Part = Convert.ToInt32(item.FindElement(By.ClassName("statText--awayValue")).Text);
                    Total = s2.ServicePointsWon.Total;
                    Percent = Part * 100 / Total;
                    s2.Aces = new UnitStat(Percent, Part, Total);
                }

                else if (text.Contains("Double Faults"))
                {
                    Part = Convert.ToInt32(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    Total = s1.ServicePointsWon.Total;
                    Percent = Part * 100 / Total;
                    s1.DoubleFaults = new UnitStat(Percent, Part, Total);

                    Part = Convert.ToInt32(item.FindElement(By.ClassName("statText--awayValue")).Text);
                    Total = s2.ServicePointsWon.Total;
                    Percent = Part * 100 / Total;
                    s2.DoubleFaults = new UnitStat(Percent, Part, Total);
                }

                else if (text.Contains("1st Serve Percentage"))
                {
                    Percent = Convert.ToInt32(item.FindElement(By.ClassName("statText--homeValue")).Text.Trim('%'));
                    Total = s1.ServicePointsWon.Total;
                    Part = Percent * Total / 100;
                    s1.FirstServePercentage = new UnitStat(Percent, Part, Total);

                    Percent = Convert.ToInt32(item.FindElement(By.ClassName("statText--awayValue")).Text.Trim('%'));
                    Total = s2.ServicePointsWon.Total;
                    Part = Percent * Total / 100;
                    s2.FirstServePercentage = new UnitStat(Percent, Part, Total);
                }

                else if (text.Contains("Winners"))
                {
                    Part = Convert.ToInt32(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    Total = s1.TotalPointsWon.Total;
                    Percent = Part * 100 / Total;
                    s1.Aces = new UnitStat(Percent, Part, Total);

                    Part = Convert.ToInt32(item.FindElement(By.ClassName("statText--awayValue")).Text);
                    Total = s2.TotalPointsWon.Total;
                    Percent = Part * 100 / Total;
                    s2.Aces = new UnitStat(Percent, Part, Total);
                }

                else if (text.Contains("Unforced Errors"))
                {
                    Part = Convert.ToInt32(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    Total = s1.TotalPointsWon.Total;
                    Percent = Part * 100 / Total;
                    s1.Aces = new UnitStat(Percent, Part, Total);

                    Part = Convert.ToInt32(item.FindElement(By.ClassName("statText--awayValue")).Text);
                    Total = s2.TotalPointsWon.Total;
                    Percent = Part * 100 / Total;
                    s2.Aces = new UnitStat(Percent, Part, Total);
                }

                else if (text.Contains("Max Points In Row"))
                {
                    s1.MaxPointsInRow = Convert.ToInt32(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    s2.MaxPointsInRow = Convert.ToInt32(item.FindElement(By.ClassName("statText--awayValue")).Text);
                }

                else if (text.Contains("Max Games In Row"))
                {
                    s1.MaxGamesInRow = Convert.ToInt32(item.FindElement(By.ClassName("statText--homeValue")).Text);
                    s2.MaxGamesInRow = Convert.ToInt32(item.FindElement(By.ClassName("statText--awayValue")).Text);
                }
            }
        }
    }
}