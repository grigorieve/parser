﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace ParserFlashscoresWF2
{
    class ParsFromClass
    {
        static IWebDriver browser = new OpenQA.Selenium.Chrome.ChromeDriver();
        static List<IWebElement> elements;
        static WebDriverWait wait = new WebDriverWait(browser, TimeSpan.FromSeconds(10));
        static List<Match> Matches = new List<Match>();

        static List<string> texts = new List<string>();
        static string url;

        static public void CreateMatchClass(string id)
        {
            url = "https://www.flashscores.co.uk/match/" + id + "/#match-summary";
            browser.Navigate().GoToUrl(url);
            wait.Until(driver => driver.FindElements(By.ClassName("fleft")).Count > 0);
            Thread.Sleep(1000);

            Match match = ParsMatchSummary.MS(browser, id);

            //Счет выраженый в геймах для каждого сэта
            // По идее, этот блок должен быть расположен в очках, но чтобы не переключать вкладку, спарсил его сразу
            elements = browser.FindElements(By.CssSelector(".scoreboard")).ToList();
            int CountOfSet = browser.FindElements(By.CssSelector(".part span")).Count / 2;

            int[,] ScoreOnGame = new int[2, CountOfSet];
            for (int i = 1; i <= CountOfSet; i++)
            {
                ScoreOnGame[0, i - 1] = int.Parse(browser.FindElement(By.ClassName("p" + i + "_home")).Text);
                ScoreOnGame[1, i - 1] = int.Parse(browser.FindElement(By.ClassName("p" + i + "_away")).Text);
            }

            // блок для заполнения статистики
            browser.FindElement(By.Id("li-match-statistics")).Click();
            wait.Until(driver => driver.FindElements(By.ClassName("statRow")).Count > 0);
            ParsStatistics.Player(browser, CountOfSet, out match.First, out match.Second);

            // блок для заполнения очков
            browser.FindElement(By.Id("li-match-history")).Click();
            wait.Until(driver => driver.FindElements(By.Id("tab-mhistory-1-history")).Count > 0);
            match.PointByPoint = ParsPointByPoint.PBP(browser, CountOfSet, ScoreOnGame, match.First.Name, match.Second.Name);

            // блок для заполнения коэффициентов
            browser.FindElement(By.Id("a-match-odds-comparison")).Click();
            wait.Until(driver => driver.FindElements(By.Id("a-match-odds-comparison")).Count > 0);

            Thread.Sleep(1000);
            browser.FindElement(By.CssSelector(".odds-comparison-spacer .eu")).Click();

            match.OddsTitles = ParsOdds.PO(browser);

            // Добавление матча в список матчей
            Matches.Add(match);

            double x = Matches[0].OddsTitles[0].stages[1].markets[0].units[1].CoefD[0];

        }
    }
}
