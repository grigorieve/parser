﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ParserFlashscoresWF2
{
    static class ParserInMatch
    {
        static IWebDriver browser = new OpenQA.Selenium.Chrome.ChromeDriver();
        static IWebElement element;
        static List<IWebElement> elements;
        static List<IWebElement> Categories;
        static List<IWebElement> Citys;
        static WebDriverWait wait = new WebDriverWait(browser, TimeSpan.FromSeconds(5));

        static string text = "";
        static List<string> texts = new List<string>();
        static string url;

        static public string MatchSummary(string id)
        {
            url = "https://www.flashscores.co.uk/match/" + id + "/#match-summary";
            browser.Navigate().GoToUrl(url);

            //wait.Until(ExpectedConditions.ElementExists(By.ClassName("headerStrip")));
            wait.Until(driver => driver.FindElements(By.ClassName("headerStrip")).Count > 0);

            element = browser.FindElement(By.ClassName("headerStrip"));
            text = element.Text + "\r\n  ++++++++++ \r\n";
            element = browser.FindElement(By.ClassName("team-primary-content"));
            text += element.Text + "\r\n  ++++++++++ \r\n";
            element = browser.FindElement(By.ClassName("match-time"));
            text += element.Text + "\r\n  ++++++++++ \r\n";

            return text;
        }

        static public string MatchStatistics(string id)
        {
            url = "https://www.flashscores.co.uk/match/" + id + "/#match-statistics;0";
            browser.Navigate().GoToUrl(url);
            //wait.Until(ExpectedConditions.ElementExists(By.Id("tab-statistics-0-statistic")));
            wait.Until(driver => driver.FindElements(By.Id("tab-statistics-0-statistic")).Count > 0);
            element = browser.FindElement(By.Id("tab-statistics-0-statistic"));
            text = element.Text + "\r\n ++++++++++ \r\n";

            element = browser.FindElement(By.Id("statistics-1-statistic"));
            element.Click();
            wait.Until(driver => driver.FindElements(By.Id("tab-statistics-1-statistic")).Count > 0);
            element = browser.FindElement(By.Id("tab-statistics-1-statistic"));
            text += element.Text + "\r\n ++++++++++ \r\n";

            element = browser.FindElement(By.Id("statistics-2-statistic"));
            element.Click();
            wait.Until(driver => driver.FindElements(By.Id("tab-statistics-2-statistic")).Count > 0);
            element = browser.FindElement(By.Id("tab-statistics-2-statistic"));
            text += element.Text + "\r\n ++++++++++ \r\n";

            return text;
        }

        static public string PointByPoont(string id)
        {
            url = "https://www.flashscores.co.uk/match/" + id + "/#point-by-point;1";
            browser.Navigate().GoToUrl(url);
            wait.Until(driver => driver.FindElements(By.Id("tab-mhistory-1-history")).Count > 0);
            element = browser.FindElement(By.Id("tab-mhistory-1-history"));
            text = element.Text + "\r\n ++++++++++ \r\n";

            element = browser.FindElement(By.Id("mhistory-2-history"));
            element.Click();
            wait.Until(driver => driver.FindElements(By.Id("tab-mhistory-2-history")).Count > 0);
            element = browser.FindElement(By.Id("tab-mhistory-2-history"));
            text += element.Text + "\r\n ++++++++++ \r\n";

            return text;
        }

        static public string Odds(string id)
        {
            url = "https://www.flashscores.co.uk/match/" + id + "/#odds-comparison;home-away;full-time";
            browser.Navigate().GoToUrl(url);
            wait.Until(driver => driver.FindElements(By.Id("block-moneyline-ft")).Count > 0);
            element = browser.FindElement(By.Id("block-moneyline-ft"));
            text = element.Text + "\r\n ++++++++++ \r\n";

            element = browser.FindElement(By.Id("bookmark-moneyline-set1"));
            element.Click();
            wait.Until(driver => driver.FindElements(By.Id("block-moneyline-set1")).Count > 0);
            element = browser.FindElement(By.Id("block-moneyline-set1"));
            text += element.Text + "\r\n ++++++++++ \r\n";

            element = browser.FindElement(By.Id("bookmark-under-over"));
            element.Click();
            wait.Until(driver => driver.FindElements(By.Id("block-under-over-ft")).Count > 0);
            element = browser.FindElement(By.Id("block-under-over-ft"));
            text += element.Text + "\r\n ++++++++++ \r\n";

            element = browser.FindElement(By.Id("bookmark-under-over-set1"));
            element.Click();
            wait.Until(driver => driver.FindElements(By.Id("block-under-over-set1")).Count > 0);
            element = browser.FindElement(By.Id("block-under-over-set1"));
            text += element.Text + "\r\n ++++++++++ \r\n";

            return text;
        }

        static public string ParsCategories()
        {
            browser.Navigate().GoToUrl("https://www.flashscore.com/tennis/");
            element = browser.FindElement(By.CssSelector(".show-more:nth-child(12) > a"));
            element.Click();

            Categories = browser.FindElements(By.CssSelector("li[id*=lmenu")).ToList();

            text = "";

            foreach (var item in Categories)
            {
                text += item.Text + "\r\n";
            }

            return text;
        }

        static public string ParsAllCitys() 
        {
            ParsCategories();

            for (int i = 0; i < Categories.Count; i++)
            {
                Categories[i].Click();
                Thread.Sleep(1000);
            }

            //foreach (var item in Categories)
            //{
            //    item.Click();

            //    Thread.Sleep(1000);
            //}

            Citys = browser.FindElements(By.CssSelector("span[title*=Add]")).ToList();

            foreach (var item2 in Citys)
            {
                text += item2.Text;
            }


            return text;
        }

        static public string ParsCity()
        {
            browser.Navigate().GoToUrl("https://www.flashscore.com/tennis/");

            Thread.Sleep(3000);

            element = browser.FindElement(By.Id("lmenu_5724"));
            element.Click();

            elements = browser.FindElements(By.CssSelector("#lmenu_5724 a")).ToList();
            foreach (var item in elements)
            {
                texts.Add(item.GetAttribute("href"));
            }

            //element = browser.FindElement(By.ClassName("lmenu_5724"));
            //text = element.Text + "\r\n  ++++++++++ \r\n";

            return text;
        }

        static public string ParsMatches()
        {
            browser.Navigate().GoToUrl("https://www.flashscores.co.uk/tennis/atp-singles/brisbane/results/"); // + results/

            Thread.Sleep(3000);

            elements = browser.FindElements(By.ClassName("stage-finished")).ToList();

            text = "";
            foreach (var item in elements)
            {
                texts.Add(item.GetAttribute("id"));
                //text += item.GetAttribute("id") + "\r\n";
                text += item.Text + "\r\n";
            }

            element = browser.FindElement(By.ClassName("stage-finished"));
            //elements = browser.FindElements(By.ClassName("tennis")).ToList();
            //text = element.Text + "\r\n  ++++++++++ \r\n";

            return text;
        }
    }
}
