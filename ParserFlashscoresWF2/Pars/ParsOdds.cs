﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace ParserFlashscoresWF2
{
    class ParsOdds
    {
        static List<IWebElement> titles;

        internal static List<OddsTitle> PO(IWebDriver browser)
        {
            List<OddsTitle> oddsTitles = new List<OddsTitle>();

            Thread.Sleep(1000);

            titles = browser.FindElements(By.CssSelector(".odds-comparison-bookmark [id^=bookmark]")).ToList();
            foreach (var t in titles)
            {
                oddsTitles.Add(new OddsTitle(browser, t));
            }
            return oddsTitles;
        }
    }
}
