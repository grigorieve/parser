﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ParserFlashscoresWF2
{
    class ParsMatchSummary
    {
        static IWebElement element;
        static List<IWebElement> elements;
        static string text = "";

        public static Match MS(IWebDriver browser, string id)
        {
            Match match = new Match(id);

            element = browser.FindElement(By.ClassName("fleft"));
            text = element.Text;

            // WTA - SINGLES: Cincinnati (USA), hard - 1/8-finals
            // ATP - SINGLES: Australian Open (Australia), hard - Quarter-finals
            // ATP - SINGLES: Next Gen Finals - Milan (World), hard (indoor)
            // ATP - SINGLES: Davis Cup - Group II (World)
            // WTA - SINGLES: Kuala Lumpurn (Malaysia), hard - Quarter-finals
            // WTA - SINGLES: Rio de Janeiro (Brazil) - Qualification, clay - Semi-finals
            // EXHIBITION - MEN: King Salman (Saudi Arabia), hard (indoor)
            // text = "ATP - SINGLES: Australian Open (Australia), hard - Quarter-finals";

            // с начала строки до двоеточия
            Regex regex = new Regex(@"(.*):");
            match.Categorie = regex.Match(text).Groups[1].Value;

            // после двоеточия, до открывающейся скобки
            regex = new Regex(@":\ (.*?) \(");
            match.City = regex.Match(text).Groups[1].Value;

            // в скобках
            regex = new Regex(@"\((.*?)\)"); // нужно выбрать только первое вхождение
            match.Country = regex.Match(text).Groups[1].Value;

            // после запятой, один из вариантов (clay, grass, hard, carpet)
            regex = new Regex(@", (.*?) -"); // до  - или конца строки
            match.Cover = regex.Match(text).Groups[1].Value;

            regex = new Regex(@"- (.*)\- (.+)"); // переделать
            match.TournamentStage = regex.Match(text).Groups[2].Value;
            if (text.Contains("Qualification")) match.TournamentStage = "Qualification " + match.TournamentStage;

            element = browser.FindElement(By.Id("utime"));
            text = element.Text;
            match.StartTime = DateTime.ParseExact(text, "dd.MM.yyyy HH:mm", null);

            // временные промежутки, потом их надо добавить в статистику
            elements = browser.FindElements(By.CssSelector(".match-time .score")).ToList();
            foreach (var item in elements)
            {
                if (item.Text == "") break;
                TimeSpan time = TimeSpan.Parse(item.Text);
                match.durartions.Add(time);
            }

            return match;
        }
    }
}
