﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserFlashscoresWF2
{
    class Match
    {
        // блок Summary
        string IDMatch;

        public Match(string id)
        {
            IDMatch = id;
        }

        public string Categorie { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Cover { get; set; }  //покрытие, возможно надо сделать список
        public string TournamentStage { get; set; }
        public DateTime StartTime { get; set; }
        public List<TimeSpan> durartions = new List<TimeSpan>(); // длительности матча, сэтов

        // блок Statistics
        public Player First = new Player();
        public Player Second = new Player();

        // блок Points
        public PointByPoint PointByPoint = new PointByPoint();

        // блок Odds
        public List<OddsTitle> OddsTitles = new List<OddsTitle>();
    }
}
