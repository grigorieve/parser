﻿namespace ParserFlashscoresWF2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbEnterUrl = new System.Windows.Forms.TextBox();
            this.tbResultPars = new System.Windows.Forms.TextBox();
            this.btnMatchSummary = new System.Windows.Forms.Button();
            this.btnMatchStatistics = new System.Windows.Forms.Button();
            this.btnPointByPoont = new System.Windows.Forms.Button();
            this.btnOdds = new System.Windows.Forms.Button();
            this.btnCategories = new System.Windows.Forms.Button();
            this.btnCity = new System.Windows.Forms.Button();
            this.btnMatches = new System.Windows.Forms.Button();
            this.btnQuit = new System.Windows.Forms.Button();
            this.lblID = new System.Windows.Forms.Label();
            this.btnGetID = new System.Windows.Forms.Button();
            this.btnAllMatches = new System.Windows.Forms.Button();
            this.btnAllCity = new System.Windows.Forms.Button();
            this.btnCreateClass = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbEnterUrl
            // 
            this.tbEnterUrl.Location = new System.Drawing.Point(12, 12);
            this.tbEnterUrl.Name = "tbEnterUrl";
            this.tbEnterUrl.Size = new System.Drawing.Size(503, 20);
            this.tbEnterUrl.TabIndex = 0;
            this.tbEnterUrl.Text = "https://www.flashscores.co.uk/match/W0lxZlWa/#match-summary";
            this.tbEnterUrl.TextChanged += new System.EventHandler(this.tbEnterUrl_TextChanged);
            // 
            // tbResultPars
            // 
            this.tbResultPars.Location = new System.Drawing.Point(275, 38);
            this.tbResultPars.Multiline = true;
            this.tbResultPars.Name = "tbResultPars";
            this.tbResultPars.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbResultPars.Size = new System.Drawing.Size(356, 361);
            this.tbResultPars.TabIndex = 1;
            // 
            // btnMatchSummary
            // 
            this.btnMatchSummary.Location = new System.Drawing.Point(12, 37);
            this.btnMatchSummary.Name = "btnMatchSummary";
            this.btnMatchSummary.Size = new System.Drawing.Size(257, 23);
            this.btnMatchSummary.TabIndex = 2;
            this.btnMatchSummary.Text = "MatchSummary";
            this.btnMatchSummary.UseVisualStyleBackColor = true;
            this.btnMatchSummary.Click += new System.EventHandler(this.btnMatchSummary_Click);
            // 
            // btnMatchStatistics
            // 
            this.btnMatchStatistics.Location = new System.Drawing.Point(12, 66);
            this.btnMatchStatistics.Name = "btnMatchStatistics";
            this.btnMatchStatistics.Size = new System.Drawing.Size(257, 23);
            this.btnMatchStatistics.TabIndex = 3;
            this.btnMatchStatistics.Text = "MatchStatistics";
            this.btnMatchStatistics.UseVisualStyleBackColor = true;
            this.btnMatchStatistics.Click += new System.EventHandler(this.btnMatchStatistics_Click);
            // 
            // btnPointByPoont
            // 
            this.btnPointByPoont.Location = new System.Drawing.Point(12, 95);
            this.btnPointByPoont.Name = "btnPointByPoont";
            this.btnPointByPoont.Size = new System.Drawing.Size(257, 23);
            this.btnPointByPoont.TabIndex = 4;
            this.btnPointByPoont.Text = "PointByPoont";
            this.btnPointByPoont.UseVisualStyleBackColor = true;
            this.btnPointByPoont.Click += new System.EventHandler(this.btnPointByPoont_Click);
            // 
            // btnOdds
            // 
            this.btnOdds.Location = new System.Drawing.Point(12, 124);
            this.btnOdds.Name = "btnOdds";
            this.btnOdds.Size = new System.Drawing.Size(257, 23);
            this.btnOdds.TabIndex = 5;
            this.btnOdds.Text = "Odds";
            this.btnOdds.UseVisualStyleBackColor = true;
            this.btnOdds.Click += new System.EventHandler(this.btnOdds_Click);
            // 
            // btnCategories
            // 
            this.btnCategories.Location = new System.Drawing.Point(12, 172);
            this.btnCategories.Name = "btnCategories";
            this.btnCategories.Size = new System.Drawing.Size(257, 23);
            this.btnCategories.TabIndex = 6;
            this.btnCategories.Text = "Categories (Tennis)";
            this.btnCategories.UseVisualStyleBackColor = true;
            this.btnCategories.Click += new System.EventHandler(this.btnCategories_Click);
            // 
            // btnCity
            // 
            this.btnCity.Location = new System.Drawing.Point(12, 201);
            this.btnCity.Name = "btnCity";
            this.btnCity.Size = new System.Drawing.Size(257, 23);
            this.btnCity.TabIndex = 7;
            this.btnCity.Text = "City (ATP - Singles)";
            this.btnCity.UseVisualStyleBackColor = true;
            this.btnCity.Click += new System.EventHandler(this.btnCity_Click);
            // 
            // btnMatches
            // 
            this.btnMatches.Location = new System.Drawing.Point(12, 230);
            this.btnMatches.Name = "btnMatches";
            this.btnMatches.Size = new System.Drawing.Size(257, 23);
            this.btnMatches.TabIndex = 8;
            this.btnMatches.Text = "Matches (ATP Brisbane)";
            this.btnMatches.UseVisualStyleBackColor = true;
            this.btnMatches.Click += new System.EventHandler(this.btnMatches_Click);
            // 
            // btnQuit
            // 
            this.btnQuit.Location = new System.Drawing.Point(12, 375);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(257, 23);
            this.btnQuit.TabIndex = 9;
            this.btnQuit.Text = "Quit";
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // lblID
            // 
            this.lblID.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblID.AutoSize = true;
            this.lblID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblID.Location = new System.Drawing.Point(561, 15);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(65, 13);
            this.lblID.TabIndex = 10;
            this.lblID.Text = "W0lxZlWa";
            // 
            // btnGetID
            // 
            this.btnGetID.Location = new System.Drawing.Point(521, 12);
            this.btnGetID.Name = "btnGetID";
            this.btnGetID.Size = new System.Drawing.Size(34, 20);
            this.btnGetID.TabIndex = 11;
            this.btnGetID.Text = "=>";
            this.btnGetID.UseVisualStyleBackColor = true;
            this.btnGetID.Click += new System.EventHandler(this.btnGetID_Click);
            // 
            // btnAllMatches
            // 
            this.btnAllMatches.Location = new System.Drawing.Point(12, 346);
            this.btnAllMatches.Name = "btnAllMatches";
            this.btnAllMatches.Size = new System.Drawing.Size(124, 23);
            this.btnAllMatches.TabIndex = 13;
            this.btnAllMatches.Text = "Matches (All matches)";
            this.btnAllMatches.UseVisualStyleBackColor = true;
            // 
            // btnAllCity
            // 
            this.btnAllCity.Location = new System.Drawing.Point(144, 346);
            this.btnAllCity.Name = "btnAllCity";
            this.btnAllCity.Size = new System.Drawing.Size(125, 23);
            this.btnAllCity.TabIndex = 12;
            this.btnAllCity.Text = "City (All city)";
            this.btnAllCity.UseVisualStyleBackColor = true;
            this.btnAllCity.Click += new System.EventHandler(this.btnAllCity_Click);
            // 
            // btnCreateClass
            // 
            this.btnCreateClass.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnCreateClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCreateClass.Location = new System.Drawing.Point(91, 273);
            this.btnCreateClass.Name = "btnCreateClass";
            this.btnCreateClass.Size = new System.Drawing.Size(90, 48);
            this.btnCreateClass.TabIndex = 14;
            this.btnCreateClass.Text = "Create Class";
            this.btnCreateClass.UseVisualStyleBackColor = false;
            this.btnCreateClass.Click += new System.EventHandler(this.btnCreateClass_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 410);
            this.Controls.Add(this.btnCreateClass);
            this.Controls.Add(this.btnAllMatches);
            this.Controls.Add(this.btnAllCity);
            this.Controls.Add(this.btnGetID);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.btnMatches);
            this.Controls.Add(this.btnCity);
            this.Controls.Add(this.btnCategories);
            this.Controls.Add(this.btnOdds);
            this.Controls.Add(this.btnPointByPoont);
            this.Controls.Add(this.btnMatchStatistics);
            this.Controls.Add(this.btnMatchSummary);
            this.Controls.Add(this.tbResultPars);
            this.Controls.Add(this.tbEnterUrl);
            this.Name = "Form1";
            this.Text = "Parser Tennis";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbEnterUrl;
        private System.Windows.Forms.TextBox tbResultPars;
        private System.Windows.Forms.Button btnMatchSummary;
        private System.Windows.Forms.Button btnMatchStatistics;
        private System.Windows.Forms.Button btnPointByPoont;
        private System.Windows.Forms.Button btnOdds;
        private System.Windows.Forms.Button btnCategories;
        private System.Windows.Forms.Button btnCity;
        private System.Windows.Forms.Button btnMatches;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Button btnGetID;
        private System.Windows.Forms.Button btnAllMatches;
        private System.Windows.Forms.Button btnAllCity;
        private System.Windows.Forms.Button btnCreateClass;
    }
}

